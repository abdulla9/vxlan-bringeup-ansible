# instructions
Run the scripts from the project root dir

## To deploy the switches
```bash
ANSIBLE_ROLES_PATH=/root/abdallahyas/vxlan-bringeup-ansible/roles/ ansible-playbook -e remote_server_name=localhost ./playbooks/switch_deployment/deploy_demo.yaml
```

## To deploy the tenant vxlan
```bash
ANSIBLE_ROLES_PATH=/root/abdallahyas/vxlan-bringeup-ansible/roles/ ansible-playbook -e remote_server_name=localhost -e tenant=red -e vxlan=vxlan0 -e vni=20 ./playbooks/tenant_deployment/create_tenant_bridges.yaml
```

